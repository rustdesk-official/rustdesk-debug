#[derive(Default)]
struct UI;
struct UIHostHandler;

fn main() {
    #[cfg(windows)]
    allow_err!(sciter::set_options(sciter::RuntimeOptions::GfxLayer(
        sciter::GFX_LAYER::WARP
    )));
    use sciter::SCRIPT_RUNTIME_FEATURES::*;
    allow_err!(sciter::set_options(sciter::RuntimeOptions::ScriptFeatures(
        ALLOW_FILE_IO as u8 | ALLOW_SOCKET_IO as u8 | ALLOW_EVAL as u8 | ALLOW_SYSINFO as u8
    )));
    let mut frame = sciter::WindowBuilder::main_window().create();
    #[cfg(windows)]
    allow_err!(sciter::set_options(sciter::RuntimeOptions::UxTheming(true)));
    frame.set_title("RustDesk");
    frame.event_handler(UI {});
    frame.sciter_handler(UIHostHandler {});
    let page = "index.html";
    frame.load_file(&format!(
        "file://{}/src/{}",
        std::env::current_dir()
            .map(|c| c.display().to_string())
            .unwrap_or("".to_owned()),
        page
    ));
    frame.run_app();
}

impl sciter::host::HostHandler for UIHostHandler {
    fn on_graphics_critical_failure(&mut self) {
        println!("Critical rendering error: e.g. DirectX gfx driver error. Most probably bad gfx drivers.");
    }
}

impl UI {
    fn t(&self, name: String) -> String {
        name
    }

    fn get_option(&self, _key: String) -> String {
        "".to_owned()
    }

    fn get_local_option(&self, _key: String) -> String {
        "".to_owned()
    }

    fn get_connect_status(&mut self) -> sciter::Value {
        sciter::Value::array(0)
    }
    
    fn is_can_screen_recording(&mut self, _prompt: bool) -> bool {
        false
    }
    
    fn current_is_wayland(&mut self) -> bool {
        false
    }
    
    fn is_login_wayland(&mut self) -> bool {
        false
    }
    
    fn get_app_name(&self) -> String {
        "RustDesk".to_owned()
    }
    
    fn get_error(&self) -> String {
        "".to_owned()
    }
    
    fn get_remote_id(&mut self) -> String {
        "".to_owned()
    }
    
    fn get_recent_sessions(&mut self) -> sciter::Value {
        sciter::Value::array(0)
    }
    
    fn is_installed(&self) -> bool {
        false
    }
    
    fn get_size(&mut self) -> sciter::Value {
        sciter::Value::array(0)
    }

    fn save_size(&mut self, _x: i32, _y: i32, _w: i32, _h: i32) {
    }
    
    fn get_software_update_url(&self) -> String {
        "".to_owned()
    }

    fn recent_sessions_updated(&self) -> bool { false }
}

impl sciter::EventHandler for UI {
    sciter::dispatch_script_call! {
        fn t(String);
        fn get_option(String);
        fn get_local_option(String);
        fn get_connect_status();
        fn is_can_screen_recording(bool);
        fn current_is_wayland();
        fn is_login_wayland();
        fn get_app_name();
        fn get_remote_id();
        fn get_recent_sessions();
        fn is_installed();
        fn get_size();
        fn save_size(i32, i32, i32, i32);
        fn get_error();
        fn get_software_update_url();
        fn recent_sessions_updated();
    }
}

#[macro_export]
macro_rules! allow_err {
    ($e:expr) => {
        if let Err(err) = $e {
            println!(
                "{:?}, {}:{}:{}:{}",
                err,
                module_path!(),
                file!(),
                line!(),
                column!()
            );
        } else {
        }
    };
}

